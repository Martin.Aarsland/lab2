package INF101.lab2;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

class Fridge implements IFridge {
    
	public List<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();
	public final int totalSize = 20;

	@Override
	public int nItemsInFridge() {
		if(fridgeItems == null)
			return 0;
		return fridgeItems.size();
	}

	@Override
	public int totalSize() {
		return totalSize;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if(nItemsInFridge() < totalSize()) {
			fridgeItems.add(item);
			return true;
		} //else
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		if(fridgeItems == null) {
			return;
		} 
		if(fridgeItems.isEmpty())
			throw new NoSuchElementException();

		fridgeItems.remove(item);
	}

	@Override
	public void emptyFridge() {
		fridgeItems.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		if(fridgeItems == null) {
			return null;
		} 
		if(fridgeItems.isEmpty()) {
			return null;
		}

		List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
		for (int i = nItemsInFridge() - 1; i >= 0; i--) {
			if(fridgeItems.get(i).hasExpired()) {
				expiredItems.add(fridgeItems.get(i));
				fridgeItems.remove(i);
			}
		}
		return expiredItems;

	}

}